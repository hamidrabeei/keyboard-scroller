import {
  NEW_SCROLL_TARGET,
  NEW_MOUSE_COORDS,
  Action,
  State
} from './types';

const initialState: State = {
  currentTarget: null,
  x: 0,
  y: 0
}

export const scrollReducer = (
  state = initialState,
  action: Action
): State => {
  switch (action.type) {
    case NEW_SCROLL_TARGET:
      return {
        ...state,
        currentTarget: action.target
      };
    case NEW_MOUSE_COORDS:
      return {
        ...state,
        x: action.x,
        y: action.y
      };
    default:
      return state;
  }
}
