export enum Direction {
  Up = 87,
  Down = 83,
  Left = 65,
  Right = 68 
}

export type ScrollDirection = "VERTICAL" | "HORIZONTAL";

export interface State {
  currentTarget: Element | null,
  x: number,
  y: number
}

export type Action = NewScrollTarget | NewMouseCoords;

export const NEW_SCROLL_TARGET = "NEW_SCROLL_TARGET";

interface NewScrollTarget {
  type: typeof NEW_SCROLL_TARGET,
  target: Element
}

export const NEW_MOUSE_COORDS = "NEW_MOUSE_COORDS";

interface NewMouseCoords {
  type: typeof NEW_MOUSE_COORDS,
  x: number,
  y: number
}


