import { NEW_SCROLL_TARGET, NEW_MOUSE_COORDS, Action } from './types';

export const newScrollTarget = (newTarget: Element): Action => {
  return {
    type: NEW_SCROLL_TARGET,
    target: newTarget
  };
};

export const newMouseCoords = (x: number, y: number): Action => {
  return {
    type: NEW_MOUSE_COORDS,
    x,
    y
  };
};
