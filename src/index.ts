import { Direction, ScrollDirection } from './types';
import { createStore } from 'redux';
import { scrollReducer } from './reducers';
import { newMouseCoords, newScrollTarget } from './actions';

const UNSCROLLABELS = ['SPAN', 'CODE'];
const SCROLL_AMOUNT = 400;
const store = createStore(scrollReducer);

const getScrollOpts = (
  direction: string,
  scrollAmount: number
): ScrollToOptions => {
  const top = direction === "Up"
    ? -scrollAmount
    : direction === "Down"
      ? scrollAmount
      : 0;
  const left = direction === "Right"
    ? scrollAmount / 2
    : direction === "Left"
      ? -(scrollAmount / 2)
      : 0;
  return {
    top,
    left,
    behavior: 'smooth'
  };
};


const updateMouseCoords = (x: number, y: number): void => {
  store.dispatch(newMouseCoords(x, y));
};


const isInputElemFocused = (): boolean => {
  const inputElems = ['INPUT', 'TEXTAREA'];
  const { nodeName } = document.activeElement
  return inputElems.includes(nodeName);
};


const updateTarget = (
  x: number,
  y: number
): Element => {
  const target = document.elementFromPoint(x, y);
  !target.isEqualNode(store.getState().currentTarget)
    && store.dispatch(newScrollTarget(target))
  return target;
};


const isScrollable = (direction: ScrollDirection, element: Element): boolean => {
  switch (direction) {
    case "HORIZONTAL":
      return element.scrollWidth > element.clientWidth;
    case "VERTICAL":
      return element.scrollHeight > element.clientHeight;
    default:
      return false;
  }
};


/**
 * This is used to traverse the DOM tree, when the cursor is pointed at
 * an unscrollable element, which parent is the actual element we want
 * to scroll. This is the case e.g. with span an inline code elements.
 */
const getScrollTarget = (element: Element): Element => {
  if (UNSCROLLABELS.includes(element.nodeName) && !!element.parentElement) {
    return getScrollTarget(element.parentElement);
  } else {
    return element;
  }
};


/**
 * Scrolls to element currently under the pointer, or alternatively
 * scrolls the whole page if nothing can be scrolled under pointer.
 */
const handleKeypress = (event: KeyboardEvent): void => {
  const dir = Direction[event.keyCode];
  if (!event.shiftKey || !dir || isInputElemFocused()) return;
  const scrollDir = ["Up", "Down"].includes(dir)
    ? "VERTICAL"
    : "HORIZONTAL";
  const scrollOpts = getScrollOpts(dir, SCROLL_AMOUNT);
  const { x, y } = store.getState();
  const target = updateTarget(x, y);
  const firstScrollable = getScrollTarget(target);
  const scrollTarget = isScrollable(scrollDir, firstScrollable)
    ? firstScrollable
    : window;
  scrollTarget.scrollBy(scrollOpts);
};

window.addEventListener("keydown", handleKeypress);
window.addEventListener("mousemove", ({x, y}: MouseEvent) => updateMouseCoords(x, y));
